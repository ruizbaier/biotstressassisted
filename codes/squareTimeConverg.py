'''
Convergence-in-time test 
Coupled poroelasticity and convection-reaction-diffusion
 
P2-P1-P2 discretisation for the three-field poroelasticity 
P2 elements for the tracer
Backward Euler scheme for the time discretisation
Unit square, manufactured solutions
'''


from fenics import *
import sympy2fenics as sf

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True

def strain(v): 
    return sym(grad(v))

def str2exp(s):
    return sf.sympy2exp(sf.str2sympy(s))

def DD(s):
    return D0 + D0 * exp(-beta*tr(s))

 # ********* model constants  ******* #

# Poroelasticity

lmbda = Constant(1000.)
mus    = Constant(1.)
rhos   = Constant(1.)
c0    = Constant(1./lmbda)
kappa = Constant(1.e-5)
alpha = Constant(1.)
muf   = Constant(1.)
I = Constant(((1,0),(0,1)))

#
uinf = Constant(100.)

ell = Constant(1.e-3)
poro = Constant(0.2)
D0 = Constant(1.3e-4)
beta = Constant(0.01)

# ******* Exact solutions and forcing terms for error analysis ****** #
u_str = '(uinf*sin(t)*(y*y+x*x*0.5/lmbda),uinf*sin(t)*(x*x+y*y*0.5/lmbda))'
dt_u_str = '(uinf*cos(t)*(y*y+x*x*0.5/lmbda),uinf*cos(t)*(x*x+y*y*0.5/lmbda))'

w_str = '(x**2+y**2)*sin(t)'
dt_w_str = '(x**2+y**2)*cos(t)'

p_str = '(-x**2+y**2)*sin(t)'
dt_p_str = '(-x**2+y**2)*cos(t)'

mesh = UnitSquareMesh(20,20)
n = FacetNormal(mesh)

t = 0

w_ex = Expression(str2exp(w_str), t = t, degree=3, domain=mesh)
dt_w_ex = Expression(str2exp(dt_w_str), t = t, degree=3, domain=mesh)
u_ex  = Expression(str2exp(u_str), t = t, uinf= uinf, lmbda=lmbda, degree=3, domain=mesh)
dt_u_ex  = Expression(str2exp(dt_u_str), t = t, uinf= uinf, lmbda=lmbda, degree=3, domain=mesh)

p_ex  = Expression(str2exp(p_str), t = t, degree=3, domain=mesh)
dt_p_ex = Expression(str2exp(dt_p_str), t = t, degree=3, domain=mesh)


# ********* Finite dimensional spaces ********* #

P1 = FiniteElement("CG", mesh.ufl_cell(), 1)
P2 = FiniteElement("CG", mesh.ufl_cell(), 2)
P2v = VectorElement("CG", mesh.ufl_cell(), 2)
Hh = FunctionSpace(mesh,MixedElement([P2,P2v,P1,P2]))

print("**************** Total Dofs = ", Hh.dim())

Sol = Function(Hh); dSol = TrialFunction(Hh)
w,u,phi,p = split(Sol)
th,v,psi,q = TestFunctions(Hh)

# Essential BCs - product space
bdry = MeshFunction("size_t", mesh, 1)
bdry.set_all(0)

Gamma = CompiledSubDomain("(near(x[0],0.0) || near(x[1],0.0)) && on_boundary")
Sigma = CompiledSubDomain("(near(x[0],1.0) || near(x[1],1.0)) && on_boundary")

Gamma.mark(bdry,91); Sigma.mark(bdry,92);
ds = Measure("ds", subdomain_data=bdry)


dtvec = [0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625];
nkmax = 6

ephi = []; rphi = []; eu = []; ru = []; ep = []; rp = [];
ew = []; rw = [];
ru.append(0.0); rp.append(0.0); rphi.append(0.0); rw.append(0.0);


Tfinal= 1.0;

# ***** Error analysis ***** #

for nk in range(nkmax):

    dt = dtvec[nk]
    
    print("....... Refinement level : dt = ", dt)

    t = 0
    w_ex.t = 0.0; u_ex.t = 0.0; p_ex.t = 0.0; 
    phi_ex = alpha*p_ex - lmbda*div(u_ex)

    wold = interpolate(w_ex, Hh.sub(0).collapse())
    uold   = project(u_ex, Hh.sub(1).collapse())
    phiold = project(phi_ex, Hh.sub(2).collapse())
    pold   = interpolate(p_ex, Hh.sub(3).collapse())
    
    E_w = 0.; E_u = 0.; E_p = 0.; E_phi = 0.
    
    # ********* Time loop ************* #
        
    while (t < Tfinal):

        if t + dt > Tfinal:
            dt = Tfinal - t
            t = Tfinal
        else:
            t += dt
        
        print("t=%.5f" % t)

        w_ex.t = t; dt_w_ex.t = t;
        u_ex.t = t; p_ex.t = t; dt_p_ex.t = t; 
        dt_u_ex.t = t; 
        
        phi_ex = alpha*p_ex - lmbda*div(u_ex)
        dt_phi_ex = alpha*dt_p_ex - lmbda*div(dt_u_ex)

        sigma_ex = 2*mus*strain(u_ex)- phi_ex*I

        b_ex  =  - div(sigma_ex) / rhos  

        m_ex = (c0+alpha*alpha/lmbda)*dt_p_ex \
                 - alpha/lmbda*dt_phi_ex \
                 - kappa/muf*div(grad(p_ex))

        pflux_ex = kappa/muf*grad(p_ex)

        n_ex = poro*dt_w_ex \
               + dot(dt_u_ex-pflux_ex,grad(w_ex)) \
               - div(DD(sigma_ex)*grad(w_ex)) \
               - poro*ell*w_ex

        wflux_ex = DD(sigma_ex)*grad(w_ex)
        
        u_D = project(u_ex, Hh.sub(1).collapse())
        
        bcW = DirichletBC(Hh.sub(0), w_ex, bdry, 91)
        bcU = DirichletBC(Hh.sub(1), u_D, bdry, 91)
        bcP = DirichletBC(Hh.sub(3), p_ex, bdry, 92)
        
        bcH = [bcW,bcU,bcP]
        
        # ********  Weak form ********** #

        PE = 2.0*mus*inner(strain(u),strain(v)) * dx \
             - phi * div(v) * dx \
             + (c0 + alpha*alpha/lmbda)/dt * (p-pold) * q *dx \
             - alpha/(lmbda*dt) * (phi-phiold) * q * dx \
             + kappa/muf * dot(grad(p),grad(q)) * dx \
             + 1.0/lmbda * p * psi * dx \
             - div(u) * psi * dx \
             - 1.0/lmbda * phi * psi * dx \
             - dot(b_ex,v) * dx \
             - m_ex * q * dx \
             - dot(pflux_ex,n) * q * ds(91) \
             - dot(sigma_ex*n,v) * ds(92)

        CD = poro*(w-wold)/dt*th*dx \
             + dot((u-uold)/dt-kappa/muf*grad(p),grad(w))*th*dx \
             + dot(DD(2*mus*strain(u)- phi*I)*grad(w),grad(th))*dx \
             - poro*ell*w*th*dx \
             - n_ex * th * dx \
             - dot(wflux_ex,n) * th * ds(92)

        FF = PE + CD
        Tang = derivative(FF,Sol,dSol)
        problem = NonlinearVariationalProblem(FF, Sol, bcH, J=Tang)
        solver  = NonlinearVariationalSolver(problem)
        solver.parameters['nonlinear_solver']                    = 'newton'
        solver.parameters['newton_solver']['linear_solver']      = 'petsc'#mumps'
        solver.parameters['newton_solver']['absolute_tolerance'] = 1e-6
        solver.parameters['newton_solver']['relative_tolerance'] = 1e-6
        solver.parameters['newton_solver']['maximum_iterations'] = 25
        solver.solve()
        wh,uh,phih,ph = Sol.split()

        # updating
        assign(wold,wh); assign(uold,uh)
        assign(phiold,phih); assign(pold,ph)
        
        E_w += assemble(dt*(w_ex-wh)**2*dx)
        E_u += assemble(dt*(u_ex-uh)**2*dx)
        E_phi += assemble(dt*(phi_ex-phih)**2*dx)
        E_p += assemble(dt*(p_ex-ph)**2*dx)
        

    ew.append(pow(E_w,0.5))
    eu.append(pow(E_u,0.5))
    ephi.append(pow(E_phi,0.5))
    ep.append(pow(E_p,0.5))

    if(nk>0):
        rw.append(ln(ew[nk]/ew[nk-1])/ln(dtvec[nk]/dtvec[nk-1]))
        ru.append(ln(eu[nk]/eu[nk-1])/ln(dtvec[nk]/dtvec[nk-1]))
        rphi.append(ln(ephi[nk]/ephi[nk-1])/ln(dtvec[nk]/dtvec[nk-1]))
        rp.append(ln(ep[nk]/ep[nk-1])/ln(dtvec[nk]/dtvec[nk-1]))
        

# ********  Generating error history **** #

print('dt &  E(w)  &   r(w)  &  E(u)  &   r(u)  &   E(phi)  &   r(phi)  &  E(p)  &  r(p)  ')
print('==========================================================================')

for nk in range(nkmax):
     print('%4.4g & %4.4g & %4.4g & %4.4g & %4.4g & %4.4g & %4.4g & %4.4g & %4.4g ' % (dtvec[nk], ew[nk], rw[nk], eu[nk], ru[nk], ephi[nk], rphi[nk], ep[nk], rp[nk]))


# ************* End **************** #
