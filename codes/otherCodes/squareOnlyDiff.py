from fenics import *
 
parameters["form_compiler"]["representation"] = "uflacs"
parameters["allow_extrapolation"]= True
parameters["form_compiler"]["cpp_optimize"] = True

def f(w):
    return ell*w

# ******* Import mesh and define function spaces ****** #

mesh = UnitSquareMesh(64,64,'crossed')
n = FacetNormal(mesh)
bdry = MeshFunction("size_t", mesh, 1)
bdry.set_all(0)
bot = 31; top =32; wall= 33
GBot = CompiledSubDomain("near(x[1],0.) && on_boundary")
GTop = CompiledSubDomain("near(x[1],1.) && on_boundary")
GWall = CompiledSubDomain("(near(x[0],0.) || near(x[0],1.)) && on_boundary")
GTop.mark(bdry,top); GBot.mark(bdry,bot); GWall.mark(bdry,wall)
ds = Measure("ds", subdomain_data = bdry)


#output files and options 
fileO = XDMFFile(mesh.mpi_comm(), "outputs/brain2DDiff.xdmf")
fileO.parameters['rewrite_function_mesh']=False
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

# ********* Finite dimensional spaces ********* #

# reaction-diffusion spaces
P1 = FiniteElement("CG", mesh.ufl_cell(), 1)
Mh = FunctionSpace(mesh, P1)

print("**************** Reaction-Diffusion Dofs = ", Mh.dim())

# trial and test functions

wh  = Function(Mh)
th  = TestFunction(Mh)
trial = TrialFunction(Mh) 

# time constants [s]
t=0.0; dt = 1.0; Tfinal = 500.0; 
freqSave = 10; inc = 0

# ********* model constants  ******* #

# tracer transport
D0   = Constant(1.3e-4)
beta = Constant(1.0e-2)
ell  = Constant(1.e-5)
whin = Constant(0.95)
poro = Constant(0.2)

# ********* Initial conditions ******* #

whold = interpolate(Constant(0.),Mh)

# ********* Boundaries and boundary conditions ******* #

bcW = DirichletBC(Mh, whin, bdry, top)

# ********  Weak forms ********** #

Form  = poro*(wh-whold)/dt * th * dx \
        + D0*dot(grad(wh),grad(th))*dx \
        - poro*f(wh)*th*dx 

Tang = derivative(Form, wh, trial)
problem = NonlinearVariationalProblem(Form, wh, bcW, J=Tang)
solver  = NonlinearVariationalSolver(problem)
solver.parameters['nonlinear_solver']                    = 'newton'#snes
solver.parameters['newton_solver']['linear_solver']      = 'petsc'#mumps
solver.parameters['newton_solver']['absolute_tolerance'] = 1e-6
solver.parameters['newton_solver']['relative_tolerance'] = 1e-6
solver.parameters['newton_solver']['maximum_iterations'] = 25

# ********* Time loop ************* #

while (t <= Tfinal):

    print("t=%.2f" % t)
    
    solver.solve()
    assign(whold,wh)

    if (inc % freqSave == 0):
        wh.rename("w","w"); fileO.write(wh,t)
    
    t += dt; inc += 1
    
# ************* End **************** #
