from fenics import *
 
parameters["form_compiler"]["representation"] = "uflacs"
parameters["allow_extrapolation"]= True
parameters["form_compiler"]["cpp_optimize"] = True

def strain(v): 
    return sym(grad(v))

def DD(s):
    return D0 + D0 * exp(-beta*tr(s)) # 30% faster
    #return D0*I - D0*beta*s + D0*s*s #20% slower

# ******* Import mesh and define function spaces ****** #

mesh = UnitSquareMesh(64,64,'crossed')
n = FacetNormal(mesh)
bdry = MeshFunction("size_t", mesh, 1)
bdry.set_all(0)
bot = 31; top =32; wall= 33
GBot = CompiledSubDomain("near(x[1],0.) && on_boundary")
GTop = CompiledSubDomain("near(x[1],1.) && on_boundary")
GWall = CompiledSubDomain("(near(x[0],0.) || near(x[0],1.)) && on_boundary")
GTop.mark(bdry,top); GBot.mark(bdry,bot); GWall.mark(bdry,wall)
ds = Measure("ds", subdomain_data = bdry)


#output files and options 
fileO = XDMFFile(mesh.mpi_comm(), "outputs/brain2DStressAIso.xdmf")
#fileO = XDMFFile(mesh.mpi_comm(), "outputs/brain2DStressAssisted.xdmf")
fileO.parameters['rewrite_function_mesh']=False
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

# ********* Finite dimensional spaces ********* #

P1 = FiniteElement("CG", mesh.ufl_cell(), 1)
P2v = VectorElement("CG", mesh.ufl_cell(), 2)

Hh = FunctionSpace(mesh,MixedElement([P2v,P1,P1]))
Mh = FunctionSpace(mesh, P1)
Vh = FunctionSpace(mesh, P2v)
Zh = FunctionSpace(mesh, P1)

print("**************** Total Dofs = ", Hh.dim() + Mh.dim())

# trial and test functions

Sol  = Function(Hh)
uh,phih,ph = Sol.split()
u, phi, p = TrialFunctions(Hh) 
v, psi, q = TestFunctions(Hh) 

wh  = Function(Mh)
w  = TrialFunction(Mh)
th = TestFunction(Mh)


# ********* model constants  ******* #

lmbda = Constant(1000.)
mus    = Constant(1.) #KPa
rhos   = Constant(1.) #mg/mm^3
c0    = Constant(1./lmbda)
kappa = Constant(1.e-5) # mm^2
alpha = Constant(1.)
muf   = Constant(1.)
I = Constant(((1,0),(0,1)))

D0   = Constant(1.3e-4)
beta = Constant(0.5) #
ell  = Constant(1.e-5)
win = Constant(0.95)
poro = Constant(0.2)

wold = interpolate(Constant(0.),Mh)
uold  = project(Constant((0,0)), Hh.sub(0).collapse())
phiold = project(Constant(0.), Hh.sub(1).collapse())
pold   = project(Constant(0.), Hh.sub(2).collapse())
    
# ********* Essential boundary conditions ******* #

sigmaTop = Constant(-1.) #KPa
#pFluxTop = Constant(-1.)

pin = Constant(0.133) # kPa/mm
pout = Constant(0.)
bcUbot  = DirichletBC(Hh.sub(0), Constant((0.,0.)), bdry, bot)
bcPtop  = DirichletBC(Hh.sub(2), pin, bdry, top)
bcPwall = DirichletBC(Hh.sub(2), pout, bdry, wall)

bcs = [bcUbot,bcPtop,bcPwall]
bcW = DirichletBC(Mh, win, bdry, top)

# time constants [s]
t=0.0; dt = 1.0; finalMech = 50.; Tfinal = 500.0; 
freqSave = 10; inc = 0

# ********  Weak forms ********** #

aa = 2.*mus*inner(strain(u),strain(v)) * dx \
     - phi * div(v) * dx \
     + (c0 + alpha*alpha/lmbda)/dt * p * q *dx \
     - alpha/(lmbda*dt) * phi * q * dx \
     + kappa/muf * dot(grad(p),grad(q)) * dx \
     + 1.0/lmbda * p * psi * dx \
     - div(u) * psi * dx \
     - 1.0/lmbda * phi * psi * dx #\

bb =  sigmaTop*dot(n,v) * ds(top) \
      + (c0 + alpha*alpha/lmbda)/dt * pold * q *dx \
      - alpha/(lmbda*dt) * phiold * q * dx 
      
cc = poro/dt*w*th*dx \
     + dot((uh-uold)/dt-kappa/muf*grad(ph),grad(w))*th*dx \
     + dot(DD(2*mus*strain(uh)- phih*I)*grad(w),grad(th))*dx \
     - poro*ell*w*th*dx

dd = poro/dt*wold*th*dx 


# ********* Time loop ************* #

while (t <= Tfinal):

    print("t=%.2f" % t)

    solve(cc == dd, wh, bcW)
    assign(wold,wh)

    if ( t < finalMech):

        solve(aa == bb, Sol, bcs)
        uh,phih,ph = Sol.split()
        assign(uold,uh) 
        assign(phiold,phih)
        assign(pold,ph)

    if (inc % freqSave == 0):
        wh.rename("w","w"); fileO.write(wh,t)    
        uh.rename("u","u"); fileO.write(uh,t)
        phih.rename("phi","phi"); fileO.write(phih, t)
        ph.rename("p","p"); fileO.write(ph, t)
        
    t += dt; inc += 1    

# ************* End **************** #

'''
Tang = derivative(steady, Sol, dSol)
problem = NonlinearVariationalProblem(steady, Sol, bcs, J=Tang)
solver  = NonlinearVariationalSolver(problem)
solver.parameters['nonlinear_solver']                    = 'newton'#snes
solver.parameters['newton_solver']['linear_solver']      = 'petsc'#mumps
solver.parameters['newton_solver']['absolute_tolerance'] = 1e-6
solver.parameters['newton_solver']['relative_tolerance'] = 1e-6
solver.parameters['newton_solver']['maximum_iterations'] = 25 
solver.solve()
'''
