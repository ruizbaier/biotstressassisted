from pylab import *
from scipy.special import erf


def c(z,t,D):
    return c0*(1 - erf(z/sqrt(4*D*t)))

c0 = 1
z = 1e-4
z2 = 10e-6
z3 = 20e-6

t = linspace(0,1800, 181)
D_wake = 1e-12
D_sleep = 5.3e-11    # m^2/s

Cw = c(z,t,D_wake)
Cs = c(z,t,D_sleep)

C10 = c(z2, t, D_wake)
C20 = c(z3, t, D_wake)

print('Concentration wake: ', Cw[-1])
print('Concentration sleep: ', Cs[-1])
print('Ratio: ', Cs[-1]/Cw[-1])

plot(t,Cw)
plot(t,Cs)
legend(['Wake', 'Sleep'])
xlabel('Time [s]')
ylabel('Concentration percent of boundary')
show()


v_increase = 0.05
r_increase = v_increase**(1/3.)

grad_u = r_increase


mu = 1000
lamda = 1000*mu
alpha = 1
p = 11*133.33       # typical fluid pressure [Pa]

def sigma(grad_u, p):
    return 2*mu*grad_u + lamda*grad_u - alpha*p

sig_sleep = sigma(grad_u, p)
sig_wake = sigma(0, p)
beta = 1e-5

print('Sleep: ', exp(beta*sig_sleep), 'Wake : ',  exp(beta*sig_wake))

