'''
Convergence-in-space test 
Coupled poroelasticity and convection-reaction-diffusion
 
P2-P1-P2 discretisation for the three-field poroelasticity 
P2 elements for the tracer
Backward Euler scheme for the time discretisation
Unit square, manufactured solutions
'''


from fenics import *
import sympy2fenics as sf

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True

def strain(v): 
    return sym(grad(v))

def str2exp(s):
    return sf.sympy2exp(sf.str2sympy(s))

def DD(s):
    return D0 + D0 * exp(-beta*tr(s))

# time constants
dt = 0.1; Tfinal = 1.*dt;

# ********* model constants  ******* #

# Poroelasticity

lmbda = Constant(1000.)
mus    = Constant(1.)
rhos   = Constant(1.)
c0    = Constant(1./lmbda)
kappa = Constant(1.e-5)
alpha = Constant(1.)
muf   = Constant(1.)
I = Constant(((1,0),(0,1)))
uinf = Constant(0.1)

ell = Constant(1.e-3)
poro = Constant(0.2)
D0 = Constant(1.3e-4)
beta = Constant(0.01)

# ******* Exact solutions for error analysis ****** #
u_str = '(uinf*(sin(pi*x)*cos(pi*y)+x*x*0.5/lmbda)*t,uinf*(-cos(pi*x)*sin(pi*y)+y*y*0.5/lmbda)*t)'
dt_u_str = '(uinf*(sin(pi*x)*cos(pi*y)+x*x*0.5/lmbda),uinf*(-cos(pi*x)*sin(pi*y)+y*y*0.5/lmbda))'

w_str = 't*(exp(x)+cos(pi*x)*cos(pi*y))'
dt_w_str = '(exp(x)+cos(pi*x)*cos(pi*y))'

p_str = '(x**3-y**4)*t'
dt_p_str = 'x**3-y**4'

nkmax = 7; s = 1

hh = []; nn = []; ephi = []; rphi = []; eu = []; ru = []; ep = []; rp = [];
ew = []; rw = [];
ru.append(0.0); rp.append(0.0); rphi.append(0.0); rw.append(0.0)

# ***** Error analysis ***** #

for nk in range(nkmax):
    print("....... Refinement level : nk = ", nk)
    nps = pow(2,nk+1); mesh = UnitSquareMesh(nps,nps)
    n = FacetNormal(mesh)

    hh.append(mesh.hmax())

    t = 0

    w_ex = Expression(str2exp(w_str), t = t, degree=6, domain=mesh)
    dt_w_ex = Expression(str2exp(dt_w_str), t = t, degree=6, domain=mesh)
    u_ex  = Expression(str2exp(u_str), t = t, uinf= uinf, lmbda=lmbda, degree=6, domain=mesh)

    dt_u_ex  = Expression(str2exp(dt_u_str), t = t, uinf= uinf, lmbda=lmbda, degree=6, domain=mesh)

    p_ex  = Expression(str2exp(p_str), t = t, degree=6, domain=mesh)
    dt_p_ex = Expression(str2exp(dt_p_str), t = t, degree=6, domain=mesh)

    phi_ex = alpha*p_ex - lmbda*div(u_ex)
    dt_phi_ex = alpha*dt_p_ex - lmbda*div(dt_u_ex)

    sigma_ex = 2*mus*strain(u_ex)- phi_ex*I

    b_ex  =  - div(sigma_ex) / rhos  

    m_ex = (c0+alpha*alpha/lmbda)*dt_p_ex \
           - alpha/lmbda*dt_phi_ex \
           - kappa/muf*div(grad(p_ex))

    pflux_ex = kappa/muf*grad(p_ex)

    n_ex = poro*dt_w_ex \
           + dot(dt_u_ex-pflux_ex,grad(w_ex)) \
           - div(DD(sigma_ex)*grad(w_ex)) \
           - poro*ell*w_ex

    wflux_ex = DD(sigma_ex)*grad(w_ex)

    # ********* Finite dimensional spaces ********* #

    P1 = FiniteElement("CG", mesh.ufl_cell(), s)
    P2 = FiniteElement("CG", mesh.ufl_cell(), s+1)
    P2v = VectorElement("CG", mesh.ufl_cell(), s+1)
    Hh = FunctionSpace(mesh,MixedElement([P2,P2v,P1,P2]))

    print("**************** Total Dofs = ", Hh.dim())

    nn.append(Hh.dim())
    
    Sol = Function(Hh); dSol = TrialFunction(Hh)
    w,u,phi,p = split(Sol)
    th,v,psi,q = TestFunctions(Hh)

    # Essential BCs - product space
    bdry = MeshFunction("size_t", mesh, 1)
    bdry.set_all(0)

    Gamma = CompiledSubDomain("(near(x[0],0.0) || near(x[1],0.0)) && on_boundary")
    Sigma = CompiledSubDomain("(near(x[0],1.0) || near(x[1],1.0)) && on_boundary")

    Gamma.mark(bdry,91); Sigma.mark(bdry,92);
    ds = Measure("ds", subdomain_data=bdry)
        
    wold = interpolate(w_ex, Hh.sub(0).collapse())
    uold   = project(u_ex, Hh.sub(1).collapse())
    phiold = project(phi_ex, Hh.sub(2).collapse())
    pold   = interpolate(p_ex, Hh.sub(3).collapse())

    # ********  Weak form ********** #

    PE = 2.0*mus*inner(strain(u),strain(v)) * dx \
         - phi * div(v) * dx \
         + (c0 + alpha*alpha/lmbda)/dt * (p-pold) * q *dx \
         - alpha/(lmbda*dt) * (phi-phiold) * q * dx \
         + kappa/muf * dot(grad(p),grad(q)) * dx \
         + 1.0/lmbda * p * psi * dx \
         - div(u) * psi * dx \
         - 1.0/lmbda * phi * psi * dx \
         - dot(b_ex,v) * dx \
         - m_ex * q * dx \
         - dot(pflux_ex,n) * q * ds(91) \
         - dot(sigma_ex*n,v) * ds(92)

    CD = poro*(w-wold)/dt*th*dx \
         + dot((u-uold)/dt-kappa/muf*grad(p),grad(w))*th*dx \
         + dot(DD(2*mus*strain(u)- phi*I)*grad(w),grad(th))*dx \
         - poro*ell*w*th*dx \
         - n_ex * th * dx \
         - dot(wflux_ex,n) * th * ds(92)

    FF = PE + CD
    Tang = derivative(FF,Sol,dSol)
    
    # ********* Time loop ************* #
        
    while (t < Tfinal):

        t += dt
        
        print("t=%.5f" % t)

        w_ex.t = t; dt_w_ex.t = t;
        u_ex.t = t; p_ex.t = t; dt_p_ex.t = t; 
        dt_u_ex.t = t; 

        bcW = DirichletBC(Hh.sub(0), w_ex, bdry, 91)
        bcP = DirichletBC(Hh.sub(3), p_ex, bdry, 92)
        u_D = project(u_ex, Hh.sub(1).collapse())
        bcU = DirichletBC(Hh.sub(1), u_D, bdry, 91)
        bcH = [bcW,bcU,bcP]
        
        solve(FF == 0, Sol, J=Tang, bcs=bcH)
        wh,uh,phih,ph = Sol.split()

        assign(wold,wh); assign(uold,uh)
        assign(phiold,phih); assign(pold,ph)

    ew.append(errornorm(w_ex,wh,'H1'))
    eu.append(errornorm(u_ex,uh,'H1'))
    ephi.append(pow(assemble((phi_ex-phih)**2*dx),0.5))
    ep.append(errornorm(p_ex,ph,'H1'))

    if(nk>0):
        rw.append(ln(ew[nk]/ew[nk-1])/ln(hh[nk]/hh[nk-1]))
        ru.append(ln(eu[nk]/eu[nk-1])/ln(hh[nk]/hh[nk-1]))
        rphi.append(ln(ephi[nk]/ephi[nk-1])/ln(hh[nk]/hh[nk-1]))
        rp.append(ln(ep[nk]/ep[nk-1])/ln(hh[nk]/hh[nk-1]))
        

# ********  Generating error history **** #
print('nn   &  hh  &   e(w)  &   r(w) &   e(u)  &   r(u)  &   e(phi)  &   r(phi)  &  e(p)  &  r(p) ')
print('==========================================================================')

for nk in range(nkmax):
    print('%d & %4.4g & %4.4g & %4.4g & %4.4g & %4.4g & %4.4g & %4.4g & %4.4g & %4.4g \\\ ' % (nn[nk], hh[nk], ew[nk], rw[nk], eu[nk], ru[nk], ephi[nk], rphi[nk], ep[nk], rp[nk]))

# ************* End **************** #
