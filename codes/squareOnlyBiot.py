from fenics import *
 
parameters["form_compiler"]["representation"] = "uflacs"
parameters["allow_extrapolation"]= True
parameters["form_compiler"]["cpp_optimize"] = True

def strain(v): 
    return sym(grad(v))

# ******* Import mesh and define function spaces ****** #

mesh = UnitSquareMesh(64,64,'crossed')
n = FacetNormal(mesh)
bdry = MeshFunction("size_t", mesh, 1)
bdry.set_all(0)
bot = 31; top =32; wall= 33
GBot = CompiledSubDomain("near(x[1],0.) && on_boundary")
GTop = CompiledSubDomain("near(x[1],1.) && on_boundary")
GWall = CompiledSubDomain("(near(x[0],0.) || near(x[0],1.)) && on_boundary")
GTop.mark(bdry,top); GBot.mark(bdry,bot); GWall.mark(bdry,wall)
ds = Measure("ds", subdomain_data = bdry)


#output files and options 
fileO = XDMFFile(mesh.mpi_comm(), "outputs/brain2DBiot.xdmf")
fileO.parameters['rewrite_function_mesh']=False
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

# ********* Finite dimensional spaces ********* #

# reaction-diffusion spaces
P1 = FiniteElement("CG", mesh.ufl_cell(), 1)
# poroelasticity spaces
Bub  = FiniteElement("Bubble", mesh.ufl_cell(), 3)
P1b  = VectorElement(P1 + Bub)
P2v = VectorElement("CG", mesh.ufl_cell(), 2)
Hh = FunctionSpace(mesh,MixedElement([P2v,P1,P1]))


print("**************** Biot Dofs = ", Hh.dim())

# trial and test functions

Sol  = Function(Hh); dSol = TrialFunction(Hh)
u, phi, p = TrialFunctions(Hh) #split(Sol)
v, psi, q = TestFunctions(Hh)


# ********* model constants  ******* #

lmbda = Constant(1000.)
mus    = Constant(1.) #KPa
rhos   = Constant(1.) #mg/mm^3
c0    = Constant(1./lmbda)
kappa = Constant(1.e-5) # mm^2
alpha = Constant(1.)
muf   = Constant(1.)
I = Constant(((1,0),(0,1)))


# ********* Boundaries and boundary conditions ******* #

sigmaTop = Constant(-1.)
#pFluxTop = Constant(-1.)

pin = Constant(0.133) # kPa/mm
pout = Constant(0.)
bcUbot  = DirichletBC(Hh.sub(0), Constant((0.,0.)), bdry, bot)
bcPtop  = DirichletBC(Hh.sub(2), pin, bdry, top)
bcPwall = DirichletBC(Hh.sub(2), pout, bdry, wall)

bcs = [bcUbot,bcPtop,bcPwall]


# ********  Weak forms ********** #

steady = 2.*mus*inner(strain(u),strain(v)) * dx \
         - phi * div(v) * dx \
         + (c0 + alpha*alpha/lmbda) * p * q *dx \
         - alpha/lmbda * phi * q * dx \
         + kappa/muf * dot(grad(p),grad(q)) * dx \
         + 1.0/lmbda * p * psi * dx \
         - div(u) * psi * dx \
         - 1.0/lmbda * phi * psi * dx #\

bb =  sigmaTop*dot(n,v) * ds(top) #\
      #+ pFluxTop * q * ds(top)
       

solve(steady == bb, Sol, bcs)
'''
Tang = derivative(steady, Sol, dSol)
problem = NonlinearVariationalProblem(steady, Sol, bcs, J=Tang)
solver  = NonlinearVariationalSolver(problem)
solver.parameters['nonlinear_solver']                    = 'newton'#snes
solver.parameters['newton_solver']['linear_solver']      = 'petsc'#mumps
solver.parameters['newton_solver']['absolute_tolerance'] = 1e-6
solver.parameters['newton_solver']['relative_tolerance'] = 1e-6
solver.parameters['newton_solver']['maximum_iterations'] = 25

    
solver.solve()
'''
uh,phih,ph = Sol.split()
uh.rename("u","u"); fileO.write(uh,0.)
ph.rename("p","p"); fileO.write(ph,0.)
phih.rename("phi","phi"); fileO.write(phih,0.)


D0   = Constant(1.3e-4)
beta = Constant(0.5) #

Gh = TensorFunctionSpace(mesh,'CG',1)
sigmah = project(2*mus*strain(uh)-phih*I,Gh)
D1h = project(D0*I + D0 * exp(-beta*tr(sigmah)) * I, Gh)
D2h = project(D0*I - D0*beta*sigmah + D0*sigmah*sigmah, Gh) 

D1h.rename("D1","D1"); fileO.write(D1h,0.)
D2h.rename("D2","D2"); fileO.write(D2h,0.)
sigmah.rename("sig","sig"); fileO.write(sigmah,0.)


# ************* End **************** #
